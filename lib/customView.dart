import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class EditText extends StatelessWidget {
  final String labelTxt, errorTxt;
  final FormFieldSetter formSaved;
  final bool secureTxt;
  final TextInputType inputType;
  final ValueChanged focus;
  EditText(
      {this.labelTxt,
      this.errorTxt,
      this.formSaved,
      this.inputType,
      this.secureTxt = false,
      this.focus
      });
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: TextFormField(
        obscureText: secureTxt,
        onSaved: formSaved,
        onFieldSubmitted: focus,
        // maxLength: jarak,
        validator: (value) {
          if (value.isEmpty) {
            return errorTxt;
          }
        },
        decoration: InputDecoration(
            labelText: labelTxt,
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
        keyboardType: inputType,
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final String valueText;
  final VoidCallback fungsi;
  CustomButton({this.valueText = "Sign In", this.fungsi});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Theme.of(context).platform == TargetPlatform.iOS
          ? CupertinoButton(
              onPressed: fungsi,
              color: Colors.blue,
              child: Text(
                valueText,
                style: TextStyle(color: Colors.white),
              ),
            )
          : RaisedButton(
              onPressed: fungsi,
              color: Colors.blue,
              child: Text(
                valueText,
                style: TextStyle(color: Colors.white),
              ),
            ),
    );
  }
}
