import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<String> createWithEmailandPassword(String email, String password);
   Future<String> signWithEmailandPassword(String email, String password);
}

class Auth implements BaseAuth{
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  Future<String> createWithEmailandPassword(
      String email, String password) async {
    FirebaseUser user = await _firebaseAuth
        .createUserWithEmailAndPassword(email: email, password: password);
    return user.uid;
  }

  Future<String> signWithEmailandPassword(String email, String password)async{
    FirebaseUser user = await _firebaseAuth
            .signInWithEmailAndPassword(email: email, password: password);
            return user.uid;
  }
}
