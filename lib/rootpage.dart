import 'package:flutter/material.dart';
import 'customView.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'auth.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  String email, password;
  final _formKey = new GlobalKey<FormState>();
  bool validateSave() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    if (validateSave()) {
      try {
        FirebaseUser user = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password);
        print('Response Users ${user.uid}');
      } catch (e) {
        print('error : $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                EditText(
                  errorTxt: "Please fill your email address",
                  labelTxt: "Email Address",
                  formSaved: (value) => email = value,
                  inputType: TextInputType.emailAddress,
                ),
                EditText(
                  errorTxt: "Please fill your Password",
                  labelTxt: "Password",
                  formSaved: (value) => password = value,
                  secureTxt: true,
                  inputType: TextInputType.text,
                ),
                CustomButton(
                  fungsi: validateAndSubmit,
                  // fungsi: () {
                  //   validateSave();
                  // },
                ),
                FlatButton(
                  child: Text("Create a account in here"),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => new Registrasi()));
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Registrasi extends StatefulWidget {
  Registrasi({this.auth, this.onSignUp});
  final BaseAuth auth;
  final VoidCallback onSignUp;
  @override
  _RegistrasiState createState() => _RegistrasiState();
}

class _RegistrasiState extends State<Registrasi> {
  String email, password;
  final _formKey = new GlobalKey<FormState>();

  bool validateSaved() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    if (validateSaved()) {
      try {
        String usersID = await widget.auth.createWithEmailandPassword(email, password);
        // FirebaseUser user = await FirebaseAuth.instance
        //     .createUserWithEmailAndPassword(email: email, password: password);
        print('Registered Users UID : $usersID');
      } catch (e) {
        print('error : $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Registration"),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              EditText(
                errorTxt: "Please fill your email address",
                labelTxt: "Email Address",
                formSaved: (value) => email = value,
                inputType: TextInputType.emailAddress,
              ),
              EditText(
                errorTxt: "Please fill your Password",
                labelTxt: "Password",
                formSaved: (value) => password = value,
                secureTxt: true,
                inputType: TextInputType.text,
              ),
              CustomButton(
                valueText: "Submit",
                fungsi: validateAndSubmit,
                // fungsi: () {
                //   validateSave();
                // },
              ),
              FlatButton(
                child: Text("Already have account, click here"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
